#ifndef OPENTHERM_MASTER_H
#define OPENTHERM_MASTER_H

#include <Arduino.h>
#include <OpenTherm.h>
#include "Timers.h"

struct Opentherm_CMD_t {
  bool enableCentralHeating;
  bool enableHotWater;
  bool enableCooling;
  float setpointBoilerTemperature;
};

void setup_OT();
void loop_OT(Opentherm_CMD_t cmd);
float getCurrentBoilerTemperature();
#endif
