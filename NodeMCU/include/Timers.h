#ifndef TIMERS_H
#define TIMERS_H

#include <Arduino.h>

class t_Delay_ms {
    unsigned long start_time;
    unsigned long current_time;
    unsigned long timer_SP;
  public:
    bool started;
    bool done;
    t_Delay_ms(void); // constructor of class
    void start(unsigned long);
    void loop(void);
    void clear(void);
};
#endif
