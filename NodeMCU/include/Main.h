#include <Arduino.h>
#include "LCD_Display.h"
#include "MQTT_Client.h"
#include "OpenTherm_Master.h"
#include "General_Lib.h"

enum OperationMode {Automatic, Manual};
void update_LCD(void);
void mqqtPublish(void);
void TimedTreath(void);
void Print_ConnectionState(bool state);
