#ifndef MQTT_Client_H
#define MQTT_Client_H

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "Timers.h"

// Define functions
void setup_MQTT(void);
bool loop_MQTT(void);
float getSetpoint(void);
void MQTT_Publish(const char* Topic, const char* payload);
bool getWifiState(void);

#endif
