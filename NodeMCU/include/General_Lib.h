#include <Arduino.h>


class t_ReadButtonState {
  // Variables will change:
  int buttonState;             // the current reading from the input pin
  int lastButtonState;   // the previous reading from the input pin
  int debouncedState;

  int previousState;

  // the following variables are unsigned longs because the time, measured in
  // milliseconds, will quickly become a bigger number than can be stored in an int.
  unsigned long lastDebounceTime;  // the last time the output pin was toggled
  unsigned long debounceDelay;    // the debounce time; increase if the output flickers
  public:
    t_ReadButtonState(void); // constructor of class
    int readState(bool pin, unsigned long DebounceTime);
    bool edge(int pin);
};
