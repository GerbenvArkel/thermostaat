#ifndef LCD_DISPLAY_H
#define LCD_DISPLAY_H

#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#if defined(ARDUINO) && ARDUINO >= 100
#define printByte(args)  write(args);
#else
#define printByte(args)  print(args,BYTE);
#endif

// Declare functions
void displayKeyCodes(void);
void setup_LCD();
void LCD_Clear_All(void);
void LCD_Clear_Line (uint8_t Line);
void LCD_Print_Line(uint8_t Cursor_Pos, uint8_t Row, const char* Text);
void LCD_Print(uint8_t value);
void LCD_Print(const char* text);
#endif
