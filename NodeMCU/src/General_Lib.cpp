#include "General_Lib.h"


// Constructor of class
t_ReadButtonState::t_ReadButtonState(void){
  // init varibles
  debouncedState = HIGH;         // the current state of the output pin
  lastButtonState = LOW;   // the previous reading from the input pin

  lastDebounceTime = 0;
  debounceDelay = 0;
}

int t_ReadButtonState::readState(bool pin, unsigned long DebounceTime) {
  // read the state of the switch into a local variable:
  int reading = digitalRead(pin);

  // If the switch changed, due to noise or pressing:
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;
    }
  }
  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButtonState = reading;
  return buttonState;
}


bool t_ReadButtonState::edge(int pin){
  int currentState = digitalRead(pin);
  if (previousState == LOW && currentState == HIGH)
  {
    previousState = currentState;
    return 1;
  }
    previousState = currentState;
  return 0;
}
