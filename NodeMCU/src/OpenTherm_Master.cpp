/*
OpenTherm Communication Example Code
By: Ihor Melnyk
Date: January 19th, 2018

Uses the OpenTherm library to get/set boiler status and water temperature
Open serial monitor at 115200 baud to see output.

Hardware Connections (OpenTherm Adapter (http://ihormelnyk.com/pages/OpenTherm) to Arduino/ESP8266):
-OT1/OT2 = Boiler X1/X2
-VCC = 5V or 3.3V
-GND = GND
-IN  = Arduino (3) / ESP8266 (5) Output Pin
-OUT = Arduino (2) / ESP8266 (4) Input Pin

Controller(Arduino/ESP8266) input pin should support interrupts.
Arduino digital pins usable for interrupts: Uno, Nano, Mini: 2,3; Mega: 2, 3, 18, 19, 20, 21
ESP8266: Interrupts may be attached to any GPIO pin except GPIO16,
but since GPIO6-GPIO11 are typically used to interface with the flash memory ICs on most esp8266 modules, applying interrupts to these pins are likely to cause problems
*/

#include "OpenTherm_Master.h"

const int inPin = 12; //4
const int outPin = 14; //5
OpenTherm ot(inPin, outPin);
float actualBoilerTemp;

// Initializes timer
t_Delay_ms interval_Timer;

void handleInterrupt() {
	ot.handleInterrupt();
}

void setup_OT()
{
	ot.begin(handleInterrupt);
}

void loop_OT(Opentherm_CMD_t cmd)
{
  interval_Timer.loop();
	if (!interval_Timer.done || !interval_Timer.started){
		interval_Timer.start(2500);
		return; // exit function here if timer is not done
  }
	else interval_Timer.clear();

	//Set/Get Boiler Status
	unsigned long response = ot.setBoilerStatus(cmd.enableCentralHeating, cmd.enableHotWater, cmd.enableCooling);
	OpenThermResponseStatus responseStatus = ot.getLastResponseStatus();
	if (responseStatus == OpenThermResponseStatus::SUCCESS) {
		Serial.println("Central Heating: " + String(ot.isCentralHeatingEnabled(response) ? "on" : "off"));
		Serial.println("Hot Water: " + String(ot.isHotWaterEnabled(response) ? "on" : "off"));
		Serial.println("Flame: " + String(ot.isFlameOn(response) ? "on" : "off"));
	}
	if (responseStatus == OpenThermResponseStatus::NONE) {
		Serial.println("Error: OpenTherm is not initialized");
	}
	else if (responseStatus == OpenThermResponseStatus::INVALID) {
		Serial.println("Error: Invalid response " + String(response, HEX));
	}
	else if (responseStatus == OpenThermResponseStatus::TIMEOUT) {
		Serial.println("Error: Response timeout");
	}

  //Get Boiler Temperature
  actualBoilerTemp = ot.getBoilerTemperature();
	//Set Boiler Temperature
	ot.setBoilerTemperature(cmd.setpointBoilerTemperature);
}

float getCurrentBoilerTemperature()
{
	//Get Boiler Temperature
	return (actualBoilerTemp);
}
