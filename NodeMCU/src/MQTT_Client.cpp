/*****

 All the resources for this project:
 http://randomnerdtutorials.com/

*****/

#include "MQTT_Client.h"
#include "LCD_Display.h"

// Change the credentials below, so your ESP8266 connects to your router
const char* ssid = "H369A614FFA";
const char* password = "579A75534226";
const uint8 max_retries = 5; // Number o retries to connect with WiFi during start up

// Change the variable to your Raspberry Pi IP address, so it connects to your MQTT broker
const char* mqtt_server = "192.168.2.150";
const char* mqtt_user = "GvA";
const char* mqtt_password = "Mosqbroker";
const char* mqtt_clientid = "ESP_CV";

const char* mqtt_publish_topic = "cv/status/temperature";
const char* mqtt_subscribe_topic = "cv/command/temperature";

// Temperature setpoint received from MQTT server
float temperatureSetpoint = 0.0;

// Initializes the espClient. You should change the espClient name if you have multiple ESPs running in your home automation system
WiFiClient espClient;
PubSubClient client(espClient);

// Initializes timer
t_Delay_ms MQTT_Retry_Timer;

// Don't change the function below. This functions connects your ESP8266 to your router
void setup_wifi() {
// Logging via serial port
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

// update LCD
  LCD_Print_Line(0,1,"WiFi: Connecting...");
  WiFi.begin(ssid, password);
  uint8 attempts = 0;
  while (WiFi.status() != WL_CONNECTED && attempts < max_retries) {
    attempts +=1;
    delay(2500);
  }

  if (WiFi.status() == WL_CONNECTED){
    LCD_Clear_Line(1);
    LCD_Print_Line(0,1,"WiFi: Connected");
    // Logging via serial port
    Serial.print("WiFi connected");
    Serial.println();
    Serial.print("IP: ");
    Serial.println(WiFi.localIP());
  }
  else{
    LCD_Clear_Line(1);
    LCD_Print_Line(0,1,"WiFi: Failed");
    // Logging via serial port
    Serial.print("Connection failed");
  }
}
// This functions is executed when some device publishes a message to a topic that your ESP8266 is subscribed to
// Change the function below to add logic to your program, so when a device publishes a message to a topic that
// your ESP8266 is subscribed you can actually do something
void callback(String topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;

  for (unsigned int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  // check if message received on subscribed topic
  if(topic == mqtt_subscribe_topic){
        temperatureSetpoint = messageTemp.toFloat();
  }
}

// This functions reconnects your ESP8266 to your MQTT broker
// Change the function below if you want to subscribe to more topics with your ESP8266
void reconnect() {
  MQTT_Retry_Timer.loop();

  if (!client.connected() && (MQTT_Retry_Timer.done || !MQTT_Retry_Timer.started)) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    /*
     YOU MIGHT NEED TO CHANGE THIS LINE, IF YOU'RE HAVING PROBLEMS WITH MQTT MULTIPLE CONNECTIONS
     To change the ESP device ID, you will have to give a new name to the ESP8266.
     Here's how it looks:
       if (client.connect("ESP8266Client")) {
     You can do it like this:
       if (client.connect("ESP1_Office")) {
     Then, for the other ESP:
       if (client.connect("ESP2_Garage")) {
      That should solve your MQTT multiple connections problem
    */

    if (client.connect(mqtt_clientid , mqtt_user, mqtt_password)) {
      Serial.println("connected");
      // Subscribe or resubscribe to a topic
      // You can subscribe to more topics (to control more LEDs in this example)
      client.subscribe(mqtt_subscribe_topic);
      MQTT_Retry_Timer.clear();
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println("try again in 1 minute");

      // Restart timer if done
      if (!MQTT_Retry_Timer.done)
      {
        // Start timer for 1 minute before retrying
         MQTT_Retry_Timer.start(60000);
      }
      else{
         MQTT_Retry_Timer.clear();
      }
    }
  }
}

// The setup function sets your ESP GPIOs to Outputs, starts the serial communication at a baud rate of 115200
// Sets your mqtt broker and sets the callback function
// The callback function is what receives messages and actually controls the LEDs
void setup_MQTT() {
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  // Logging via serial port
  Serial.println();
  Serial.print("Connecting to MQTT broker with IP: ");
  Serial.println(mqtt_server);

  // update LCD
  LCD_Print_Line(0,2,"MQTT: Connecting...");
  uint8 attempts = 0;
  while (!client.connected() && attempts < max_retries) {
    attempts +=1;
    if (client.connect(mqtt_clientid , mqtt_user, mqtt_password)){
      client.subscribe(mqtt_subscribe_topic);
    }
    else{
      delay(1000);
    }
  }

  if (client.connected()){
    LCD_Clear_Line(2);
    LCD_Print_Line(0,2,"MQTT: Connected");
    // Logging via serial port
    Serial.print("Connected with MQTT broker");
  }
  else{
      LCD_Clear_Line(2);
      LCD_Print_Line(0,2,"MQTT: Failed");
      // Logging via serial port
      Serial.print("Connection with MQTT broker failed");
  }
}

bool loop_MQTT() {
  bool clientState = client.connected();
  if (!clientState) {
    reconnect();
  }
  if(!client.loop())
  {
    //client.connect(mqtt_clientid , mqtt_user, mqtt_password);
  }
  return clientState;
}

void MQTT_Publish(const char* Topic, const char* payload)
{
  client.publish(Topic, payload);
}

float getSetpoint(void){
  return temperatureSetpoint;
}

bool getWifiState(void)
{
if (WiFi.status() != WL_CONNECTED) return 0;
else return 1;

}
