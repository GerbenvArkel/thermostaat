#include "Timers.h"

// Constructor of class
t_Delay_ms::t_Delay_ms(void){
  // init varibles
  start_time = 0;
  current_time = 0;
  timer_SP = 0;
  started = false;
  done = false;
}

void t_Delay_ms::start (unsigned long delay_time) {
  if (!started){
    started = true;
    timer_SP = delay_time;
    start_time = millis();
  }
}

void t_Delay_ms::loop(void){
  current_time = millis();
  if (current_time >= (start_time + timer_SP) && started){
    done = true;
  }
  else{
    done = false;
  }
}


void t_Delay_ms::clear(void){
  started = false;
  done = false;
  timer_SP = 0;
}
