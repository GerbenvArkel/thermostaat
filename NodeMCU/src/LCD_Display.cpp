//YWROBOT
//Compatible with the Arduino IDE 1.0
//Library version:1.1

#include "LCD_Display.h"

// LCD symbols
uint8_t bell[8]  = {0x4,0xe,0xe,0xe,0x1f,0x0,0x4};
uint8_t note[8]  = {0x2,0x3,0x2,0xe,0x1e,0xc,0x0};
uint8_t clock[8] = {0x0,0xe,0x15,0x17,0x11,0xe,0x0};
uint8_t heart[8] = {0x0,0xa,0x1f,0x1f,0xe,0x4,0x0};
uint8_t duck[8]  = {0x0,0xc,0x1d,0xf,0xf,0x6,0x0};
uint8_t check[8] = {0x0,0x1,0x3,0x16,0x1c,0x8,0x0};
uint8_t cross[8] = {0x0,0x1b,0xe,0x4,0xe,0x1b,0x0};
uint8_t retarrow[8] = {	0x1,0x1,0x5,0x9,0x1f,0x8,0x4};

LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

void setup_LCD()
{
  //Wire.begin(2,0);  // SDA =   SCL=
  lcd.init();                      // initialize the lcd
  lcd.backlight();

  lcd.createChar(0, bell);
  lcd.createChar(1, note);
  lcd.createChar(2, clock);
  lcd.createChar(3, heart);
  lcd.createChar(4, duck);
  lcd.createChar(5, check);
  lcd.createChar(6, cross);
  lcd.createChar(7, retarrow);
  lcd.home();
}

// display all keycodes
void displayKeyCodes(void) {
  uint8_t i = 0;
  while (1) {
    lcd.clear();
    lcd.print("Codes 0x"); lcd.print(i, HEX);
    lcd.print("-0x"); lcd.print(i+15, HEX);
    lcd.setCursor(0, 1);
    for (int j=0; j<16; j++) {
      lcd.printByte(i+j);
    }
    i+=16;
    delay(1000);
  }
}

void LCD_Print_Line(uint8_t Cursor_Pos, uint8_t Line, const char* Text){
  lcd.setCursor(Cursor_Pos, Line);
  lcd.print(Text);
}

void LCD_Print(uint8_t value){
  lcd.print(value);
}

void LCD_Print(float value){
  lcd.print(value);
}

void LCD_Print(const char* text){
  lcd.print(text);
}

void LCD_Clear_All(){
  lcd.clear();
}


void LCD_Clear_Line (uint8_t Line){
  lcd.setCursor(0, Line);
  lcd.print("                    "); // Print 20 "empty" charaters to clear the complete line
}

void loop_LCD()
{

}
