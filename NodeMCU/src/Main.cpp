/*
  ESP8266 Blink by Simon Peter
  Blink the blue LED on the ESP-01 module
  This example code is in the public domain

  The blue LED on the ESP-01 module is connected to GPIO1
  (which is also the TXD pin; so we cannot use Serial.print() at the same time)

  Note that this sketch uses LED_BUILTIN to find the pin with the internal LED
*/
#include "Main.h"

//varibles
Opentherm_CMD_t OT_CMD;
OperationMode mode;

// Initializes timer
t_Delay_ms intervalTimerTimedTreath;

t_ReadButtonState buttonUp, buttonDown;
bool UpAndDownPressed = LOW;
char txtMode[]="Mode: Automatic";
float setpointBoilerTemperature = 45.3;
float currentBoilerTemperature = 12.4;
bool mqttClientState, wifiState;

int btnUp = LOW;
int btnDown =LOW;

// constants
const int buttonUpPin = 2;
const int buttonDownPin = 13;

// interrrupt handler
void changeInputInterrupt(){
    if (!digitalRead(buttonUpPin) && !digitalRead(buttonDownPin)){
      UpAndDownPressed = HIGH;
    }
}

void setup() {
  pinMode(buttonUpPin, INPUT);
  pinMode(buttonDownPin, INPUT);

  attachInterrupt(buttonUpPin, changeInputInterrupt, FALLING);
  attachInterrupt(buttonDownPin, changeInputInterrupt, FALLING);

  Serial.begin(115200);
  setup_LCD();
  LCD_Print_Line(0,0,"Booting...");
  setup_MQTT();
  setup_OT();
  delay(1000);
  LCD_Clear_All();
}

// the loop function runs over and over again forever
void loop() {

  btnUp = !digitalRead(buttonUpPin); // iverse because of pull up resitor
  btnDown = !digitalRead(buttonDownPin);

  if (!btnUp && !btnDown && UpAndDownPressed){
     UpAndDownPressed = LOW;
     if (mode == Automatic) { // toggle mode
       strcpy(txtMode, "Mode: Manual");
       mode = Manual;
     }
     else{
       strcpy(txtMode, "Mode: Automatic");
       mode = Automatic;
     }
  }

  switch (mode) {
    default: mode = Automatic;
    case Automatic:
      mqttClientState = loop_MQTT();
    case Manual:
      if (buttonUp.edge(buttonUpPin) && setpointBoilerTemperature < 80.0) setpointBoilerTemperature += 5.0;
      if (buttonDown.edge(buttonDownPin) && setpointBoilerTemperature > 0.0) setpointBoilerTemperature -= 5.0;
  }

  OT_CMD.setpointBoilerTemperature = setpointBoilerTemperature;
  OT_CMD.enableCentralHeating = true;
  OT_CMD.enableCooling = false;
  OT_CMD.enableHotWater = true;

  // call loop functions
  loop_OT(OT_CMD);
  // get current boiler temperture
  currentBoilerTemperature = getCurrentBoilerTemperature();

  //
  intervalTimerTimedTreath.loop();
	if (!intervalTimerTimedTreath.done || !intervalTimerTimedTreath.started){
		intervalTimerTimedTreath.start(1000);
  }
	else
  {
    intervalTimerTimedTreath.clear();
    TimedTreath();
  }
}

void TimedTreath(void){
  update_LCD();
  mqqtPublish();
  setpointBoilerTemperature = getSetpoint();
}

void update_LCD(void)
{
    LCD_Clear_All();
    LCD_Print_Line(0,0,txtMode);
    LCD_Print_Line(0,1,"SP: ");
    LCD_Print(setpointBoilerTemperature);
    LCD_Print(" CV: ");
    LCD_Print(currentBoilerTemperature);
    LCD_Print_Line(0,2,"WiFi: ");
    Print_ConnectionState(getWifiState());
    LCD_Print_Line(0,3,"MQTT: ");
    Print_ConnectionState(mqttClientState);
}

void mqqtPublish(void){
  if (mqttClientState){
      char tempstr[] = "00.0";
      dtostrf(currentBoilerTemperature,2, 1, tempstr);
      MQTT_Publish("cv/status/temperature",tempstr);

      dtostrf(setpointBoilerTemperature,2, 1, tempstr);
      MQTT_Publish("cv/debug/setpointketelwater",tempstr);

  }
}

void Print_ConnectionState(bool state){
  if (state) LCD_Print("Connected");
  else LCD_Print("Disconnected");
}
